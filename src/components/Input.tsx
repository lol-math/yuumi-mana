import React, { useCallback } from "react";

type InputProps = {
  value: number;
  onChange: (value: number) => void;
};

export const Input = ({ value, onChange }: InputProps) => {
  const _onChange = useCallback(
    (e: any) => {
      onChange(Number(e.target.value));
    },
    [onChange]
  );
  return (
    <input
      type="number"
      className="border-gray-600 border-2 p-1 rounded"
      defaultValue={value}
      onBlur={_onChange}
    />
  );
};
