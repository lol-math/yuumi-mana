import React, { useState, useMemo } from "react";
import { Input } from "./components/Input";
import {
  LineChart,
  XAxis,
  Tooltip,
  CartesianGrid,
  Line,
  YAxis,
  Label,
  Legend,
} from "recharts";
import uniqolor from "uniqolor";
import "./index.css";

const getLabel = (manaRegenPercent: number) =>
  `${manaRegenPercent * 100}% Bonus Mana Regeneration`;

const growthCalculator = (championLevel: number) =>
  (championLevel - 1) * (0.7025 + 0.0175 * (championLevel - 1));

const App = (): JSX.Element => {
  const [highLight, setHighlight] = useState("");
  const [combatDuration, setCombatDuration] = useState(120);
  const [otherSkillsManaPerSecond, setOtherSkillsManaPerSecond] = useState(0);
  const [otherSkillsManaFlat, setOtherSkillsManaFlat] = useState(0);
  const [level, setLevel] = useState(16);
  const [eLevel, setELevel] = useState(5);
  const [_eCooldown, _setECooldown] = useState([12, 11, 10, 9, 8]);
  const [_eCostFlat, _setECostFlat] = useState([40, 45, 50, 55, 60]);
  const [eCostPercent, setECostPercent] = useState(0.15);

  const [cooldownReduction, setCooldownReduction] = useState(0);

  const [baseMana, setBaseMana] = useState(400);
  const [baseManaPerLevel, setBaseManaPerLevel] = useState(45);
  const [baseManaRegenerationPer5, setBaseManaRegenerationPer5] = useState(10);
  const [
    baseManaRegenerationPer5PerLevel,
    setBaseManaRegenerationPer5PerLevel,
  ] = useState(0.4);

  const totalBaseMana = useMemo(() => {
    return baseMana + baseManaPerLevel * growthCalculator(level);
  }, [baseMana, baseManaPerLevel, level]);

  const totalBaseManaRegenerationPer5 = useMemo(() => {
    return (
      baseManaRegenerationPer5 +
      baseManaRegenerationPer5PerLevel * growthCalculator(level)
    );
  }, [baseManaRegenerationPer5, baseManaRegenerationPer5PerLevel, level]);

  const eCooldown = useMemo(() => {
    return _eCooldown[eLevel - 1] * (1 - cooldownReduction);
  }, [_eCooldown, cooldownReduction, eLevel]);

  const eCostFlat = useMemo(() => {
    return _eCostFlat[eLevel - 1];
  }, [_eCostFlat, eLevel]);

  const manaRegionAmounts = useMemo(() => {
    const result = [];

    for (
      let manaRegeneration = 5;
      manaRegeneration >= 0;
      manaRegeneration -= 0.5
    ) {
      result.push(manaRegeneration);
    }
    return result;
  }, []);

  const amountOfEs = useMemo(() => {
    const result = [];
    for (let bonusMana = 0; bonusMana < 1500; bonusMana += 100) {
      const totalMana = totalBaseMana + bonusMana;
      const oneResult = {
        bonusMana,
      };
      manaRegionAmounts.forEach((bonusManaRegenPercent) => {
        const totalManaRegenPerSecond =
          (totalBaseManaRegenerationPer5 * (1 + bonusManaRegenPercent)) / 5;

        const manaRegeneratedDuringCombat =
          totalManaRegenPerSecond * combatDuration;

        const totalECost = eCostFlat + totalMana * eCostPercent;

        const totalOtherSkillsManaExpendure =
          otherSkillsManaFlat + combatDuration * otherSkillsManaPerSecond;

        const totalManaToSpendDuringCombat =
          manaRegeneratedDuringCombat + totalMana;

        const totalEsIgnoringCooldown =
          (totalManaToSpendDuringCombat - totalOtherSkillsManaExpendure) /
          totalECost;

        const maxEs = combatDuration / eCooldown;
        // @ts-ignore
        oneResult[getLabel(bonusManaRegenPercent)] = Math.min(
          maxEs,
          totalEsIgnoringCooldown
        );
      });

      result.push(oneResult);
    }
    return result;
  }, [
    combatDuration,
    eCooldown,
    eCostFlat,
    eCostPercent,
    manaRegionAmounts,
    otherSkillsManaFlat,
    otherSkillsManaPerSecond,
    totalBaseMana,
    totalBaseManaRegenerationPer5,
  ]);

  const handleMouseEnter = (o: any) => {
    setHighlight(o.dataKey);
  };

  const handleMouseLeave = () => {
    setHighlight("");
  };

  return (
    <div className="w-2/3 mx-auto py-16 flex flex-col space-y-16">
      <div className="space-y-2 text-center">
        <h1 className="text-4xl ">Yuumi Mana Calculator</h1>
        <a
          href="https://gitlab.com/lol-math/yuumi-mana"
          className="text-center text-xs text-blue-500 hover:underline"
          rel="noreferrer"
          target="_BLANK"
        >
          source code
        </a>
        <p>
          Isn&apos;t it odd, that Yummi has a &quot;Max Mana percent&quot; skill
          cost? Surely, that would mean getting more bonus mana means you can
          cast E fewer times, right? Let&apos;s find out.
        </p>
      </div>
      <LineChart
        width={800}
        height={600}
        data={amountOfEs}
        margin={{ top: 15, right: 30, left: 20, bottom: 5 }}
      >
        <XAxis dataKey="bonusMana">
          <Label value="Bonus Mana" offset={0} position="insideBottom" />
        </XAxis>
        <YAxis>
          <Label value="# of E's" angle={-90} position="insideLeft" />
        </YAxis>
        <Legend
          onMouseEnter={handleMouseEnter}
          onMouseLeave={handleMouseLeave}
          verticalAlign="bottom"
          height={150}
          margin={{ top: 50, left: 0, right: 0, bottom: 0 }}
        />

        <Tooltip />
        <CartesianGrid stroke="#f5f5f5" />
        {manaRegionAmounts.map((bonusManaRegenPercent, index) => {
          const label = getLabel(bonusManaRegenPercent);

          return (
            <Line
              key={index}
              dataKey={label}
              name={label}
              type="monotone"
              // do * 1000 so the colors are further apart.
              // @ts-ignore
              stroke={uniqolor(index * 20).color}
              strokeOpacity={
                highLight === "" ? 1 : highLight === label ? 1 : 0.5
              }
            />
          );
        })}
      </LineChart>

      <div className="flex flex-col space-y-4">
        <h2 className="text-xl uppercase font-bold">Situational</h2>
        <div className="flex flex-col space-y-2">
          <label htmlFor="">cdr: {(cooldownReduction * 100).toFixed(0)}%</label>
          <input
            min={0}
            max={0.45}
            step={0.01}
            type="range"
            value={cooldownReduction}
            onChange={(e) => setCooldownReduction(Number(e.target.value))}
          />
        </div>

        <div className="flex flex-col space-y-2">
          <label htmlFor="">combat duration (Seconds)</label>
          <Input
            value={combatDuration}
            onChange={(val) => setCombatDuration(val)}
          ></Input>
        </div>

        <div className="flex flex-col space-y-2">
          <label htmlFor="">level: {level.toFixed(0)}</label>
          <input
            min={1}
            max={18}
            step={1}
            type="range"
            value={level}
            onChange={(e) => setLevel(Number(e.target.value))}
          />
        </div>

        <div className="flex flex-col space-y-2">
          <label htmlFor="">E level: {eLevel.toFixed(0)}</label>
          <input
            min={1}
            max={5}
            step={1}
            type="range"
            value={eLevel}
            onChange={(e) => setELevel(Number(e.target.value))}
          />
        </div>

        <div className="flex flex-col space-y-2">
          <label htmlFor="">Other Skills Mana per second</label>
          <Input
            value={otherSkillsManaPerSecond}
            onChange={(val) => setOtherSkillsManaPerSecond(val)}
          ></Input>
        </div>

        <div className="flex flex-col space-y-2">
          <label htmlFor="">Other Skills Mana (flat)</label>
          <Input
            value={otherSkillsManaFlat}
            onChange={(val) => setOtherSkillsManaFlat(val)}
          ></Input>
        </div>

        <h2 className="text-xl uppercase font-bold">
          Patch Specific (updated at 10.13)
        </h2>

        <div className="flex flex-col space-y-2">
          <label htmlFor="">E cooldown (per skill level 1-5)</label>
          {_eCooldown.map((_, index) => (
            <Input
              key={index}
              value={_eCooldown[index]}
              onChange={(val) =>
                _setECooldown((currentECooldown) => {
                  const nextECooldown = [...currentECooldown];
                  nextECooldown[index] = val;
                  return nextECooldown;
                })
              }
            ></Input>
          ))}
        </div>

        <div className="flex flex-col space-y-2">
          <label htmlFor="">E cost flat (per skill level 1-5)</label>
          {_eCostFlat.map((_, index) => (
            <Input
              key={index}
              value={_eCostFlat[index]}
              onChange={(val) =>
                _setECostFlat((currentECostFlat) => {
                  const nextECost = [...currentECostFlat];
                  nextECost[index] = val;
                  return nextECost;
                })
              }
            ></Input>
          ))}
        </div>

        <div className="flex flex-col space-y-2">
          <label htmlFor="">e cost (percent)</label>
          <Input
            value={eCostPercent}
            onChange={(val) => setECostPercent(val)}
          ></Input>
        </div>

        <div className="flex flex-col space-y-2">
          <label htmlFor="">base Mana</label>
          <Input value={baseMana} onChange={(val) => setBaseMana(val)}></Input>
        </div>

        <div className="flex flex-col space-y-2">
          <label htmlFor="">base mana per level</label>
          <Input
            value={baseManaPerLevel}
            onChange={(val) => setBaseManaPerLevel(val)}
          ></Input>
        </div>

        <div className="flex flex-col space-y-2">
          <label htmlFor="">base mana regen per 5</label>
          <Input
            value={baseManaRegenerationPer5}
            onChange={(val) => setBaseManaRegenerationPer5(val)}
          ></Input>
        </div>

        <div className="flex flex-col space-y-2">
          <label htmlFor="">base mana regen per 5 per level</label>
          <Input
            value={baseManaRegenerationPer5PerLevel}
            onChange={(val) => setBaseManaRegenerationPer5PerLevel(val)}
          ></Input>
        </div>
      </div>
    </div>
  );
};

export default App;
